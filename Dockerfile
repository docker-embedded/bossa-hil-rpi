# Pull Debian image
FROM registry.gitlab.com/docker-embedded/bossa:rpi

# Install packages
RUN apt update
RUN apt-get install -y --no-install-recommends \
		python3 \
		python3-setuptools \
		python3-pip \
		python3-dev

# Install Python packages
RUN pip3 install wheel

RUN pip3 install \
	pyserial \
	pytest \
	xunitparser \
	pyvisa \
	pylint \
	pylint-junit \
	RPi.GPIO